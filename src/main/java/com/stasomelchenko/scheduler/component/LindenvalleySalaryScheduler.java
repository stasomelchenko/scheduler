package com.stasomelchenko.scheduler.component;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class LindenvalleySalaryScheduler {

    @Scheduled(fixedRate = 5000)
    public void paySalary() {
        System.out.println("Pay Salary. Time: " + System.currentTimeMillis());
    }

}
